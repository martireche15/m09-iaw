<?php
include_once("card.php");
include_once("hand.php");
include_once("cardPile.php");

$hand = new Hand();

if ($_POST) {
    if (isset($_POST['giveCards'])) {
        $cardPile = generateCards();
        $hand = getCards($cardPile);
        showCards($hand);
        $text = getPoints($hand);
        showPoints($text);
        $total = getTotal($hand);
        showTotal($total);
        evaluate($hand);
    }
}


function generateCards(): CardPile
{
    $cardPile = new CardPile();
    $suits = [1, 2, 3, 4]; 

    $images = glob("cards/*.gif");

    $excludedImages = ["cards/card_55.gif", "cards/card_56.gif"];
    $images = array_diff($images, $excludedImages);

    $images = array_values($images);

    for ($value = 1; $value <= 13; $value++) {
        foreach ($suits as $suit) {
            $imageIndex = array_rand($images);
            $imagePath = $images[$imageIndex];

            $card = new Card($value, $suit, $imagePath);
            $cardPile->addCard($card);

            unset($images[$imageIndex]);
            $images = array_values($images);
        }
    }

    return $cardPile;
}




function getCards(CardPile &$cardPile): Hand
{
    $hand = new Hand();
    $cards = $cardPile->getPile();

    for ($i = 0; $i < 5; $i++) {
        $randomIndex = array_rand($cards);
        $hand->addCard($cards[$randomIndex]);
        unset($cards[$randomIndex]); 
    }

    return $hand;
}

function showCards(Hand $hand)
{
    $cards = $hand->getCards();
    foreach ($cards as $card) {
        $value = $card->getValue();
        $suit = $card->getSuit();
        $imagePath = $card->getImage(); 

        $suitNames = [
            1 => "Hearts",
            2 => "Diamonds",
            3 => "Clubs",
            4 => "Spades"
        ];

        $cardDescription = "{$value} of {$suitNames[$suit]}";

        echo '<img src="' . $imagePath . '" alt="' . $cardDescription . '">';
    }
}



function getPoints(&$hand): string
{
    $points = "";
    $cards = $hand->getCards(); 

    foreach ($cards as $card) {
        $value = ($card->getValue() % 13);
        if ($value == 1) {
            $value = 14;
        } elseif ($value == 11) {
            $value = 11;
        } elseif ($value == 12) {
            $value = 12;
        } elseif ($value == 0) {
            $value = 13;
        }
        $points .= $value . " ";
    }
    return trim($points);
}


function showPoints($text)
{
    echo "CARD POINTS: " . $text . "<br>";
}

function getTotal(Hand $hand): int
{
    $total = 0;
    $cards = $hand->getCards();
    foreach ($cards as $card) {
        $value = $card->getValue();
        // El valor del As es 14
        if ($value == 1) {
            $value = 14;
        }
        $total += $value;
    }
    return $total;
}

function showTotal($total)
{
    echo "TOTAL POINTS: " . $total . "<br>";
}

function evaluate(Hand $hand)
{
    $cards = $hand->getCards();
    $values = [];
    $suits = [];

    foreach ($cards as $card) {
        $values[] = $card->getValue();
        $suits[] = $card->getSuit();
    }

    // Ordenar los valores y los palos
    sort($values);
    $uniqueSuits = array_unique($suits);

    // Evaluar combinaciones de cartas
    if (isRoyalFlush($values, $uniqueSuits)) {
        echo "RESULT: ROYAL FLUSH";
    } elseif (isStraightFlush($values, $uniqueSuits)) {
        echo "RESULT: STRAIGHT FLUSH";
    } elseif (isRepoker($values)) {
        echo "RESULT: REPOKER";
    } elseif (isPoker($values)) {
        echo "RESULT: POKER";
    } elseif (isFullHouse($values)) {
        echo "RESULT: FULL HOUSE";
    } elseif (isFlush($uniqueSuits)) {
        echo "RESULT: FLUSH";
    } elseif (isStraight($values)) {
        echo "RESULT: STRAIGHT";
    } elseif (isThreeOfAKind($values)) {
        echo "RESULT: THREE OF A KIND";
    } elseif (isTwoPairs($values)) {
        echo "RESULT: TWO PAIRS";
    } elseif (isOnePair($values)) {
        echo "RESULT: ONE PAIR";
    } else {
        echo "RESULT: HIGH CARD";
    }
}

function isRoyalFlush($values, $suits): bool
{
    $royalValues = [10, 11, 12, 13, 14];
    return $values === $royalValues && count($suits) === 1;
}

function isStraightFlush($values, $suits): bool
{
    return isStraight($values) && count($suits) === 1;
}

function isRepoker($values): bool
{
    $valueCounts = array_count_values($values);
    return in_array(4, $valueCounts);
}

function isPoker($values): bool
{
    $valueCounts = array_count_values($values);
    return in_array(3, $valueCounts) && in_array(2, $valueCounts);
}

function isFullHouse($values): bool
{
    $valueCounts = array_count_values($values);
    return in_array(3, $valueCounts) && in_array(2, $valueCounts);
}

function isFlush($suits): bool
{
    return count($suits) === 1;
}

function isStraight($values): bool
{
    for ($i = 1; $i < count($values); $i++) {
        if ($values[$i] - $values[$i - 1] !== 1) {
            return false;
        }
    }
    return true;
}

function isThreeOfAKind($values): bool
{
    $valueCounts = array_count_values($values);
    return in_array(3, $valueCounts);
}

function isTwoPairs($values): bool
{
    $valueCounts = array_count_values($values);
    return count(array_keys($valueCounts, 2)) === 2;
}

function isOnePair($values): bool
{
    $valueCounts = array_count_values($values);
    return in_array(2, $valueCounts);
}
?>
