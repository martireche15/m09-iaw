<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <div class="container mt-4">
        <h1 class="text-center mb-4">POKER HAND</h1>
        <form method="POST" class="text-center mb-4">
            <button class="btn btn-primary" type="submit" name="execute">GIVE CARDS</button>
        </form>
        <p class="text-center">Click the button to get your cards.</p>

        <?php 
        include 'app-alumne.php';

        // Verificar si se ha recibido un formulario POST
        if ($_POST && isset($_POST['execute'])) {
            // Generar y mostrar las cartas
            $cardPile = generateCards();
            $hand = getCards($cardPile);

            // Mostrar las cartas
            echo '<div class="d-flex justify-content-center mb-4">';
            showCards($hand);
            echo '</div>';

            // Obtener y mostrar los puntos de las cartas
            $points = getPoints($hand);
            echo '<div class="text-center">';
            showPoints($points);

            // Calcular y mostrar el total de puntos
            $total = getTotal($hand);
            showTotal($total);

            // Evaluar la mano de poker
            evaluate($hand);
            echo '</div>';
        }
        ?>
    </div>
</body>

</html>
