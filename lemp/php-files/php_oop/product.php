<?php
class Product {
    
    // Properties
    private int $id;

    private string $name;

    private float $price;

    private array $colors;

    //Constructor
    function __construct(int $id, string $name, float $price, array $colors){   
        //this s'utilitza per diferenciar l'objecte
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->colors = $colors;
    }

    //Destructor
    function __destruct(){
        //$this = null;
        //unset($this);
    }

    //Getters and setters
    function get_id(): int {
        return $this->id; //Despres de la fletxa mai $, sempre la variable
    }

    function set_id(int $id): void{
        $this->id = $id;
    }

    function get_name(): string {
        return $this->name; 
    }

    function set_name(string $name): void{
        $this->name = $name;
    }

    function get_price(): float {
        return $this->price; 
    }

    function set_price(float $price): void{
        $this->price = $price;
    }

    function get_colors(): array{
        return $this->colors; 
    }

    function set_colors(array $colors): void{
        $this->colors = $colors;
    }

    //Class Methods (optional)

    //toString (optional)
    function tax(float $tax): float{
        return $this-> price * $tax / (1 + $tax);
    }

    function priceNoTax(float $tax): float {
        return $this->price /(1 + $tax);
    }

    function __toString(): string{
        $text = "";
        foreach ($this->colors as $color){
            $text .= $color;
        }
        return "Product[id=" . $this->id .",name=" . $this->name .", price=". $this->price ."]";
    }
}    

$colors = ["Black", "Blue", "Green"];
    
$p1 = new Product(1,"T-Shirt","12",$colors);
$p2 = new Product(1,"AK-47","3449",["black"]);

$p1->get_id() . "<br>";
$p1->get_name() . "<br>";
$p1->get_price() . "<br>";

$array_colors = $p1->get_colors();
print_r($array_colors);
echo "<br>";

var_dump($array_colors);
echo "<br>";

foreach ($colors as $color) 
{
    echo $color . " ";
}   

?>

<?php

echo "Methods: <br/>";

echo $p1 -> priceNoTax(0.21);
echo "<br>";
echo $p1 -> tax(0.21);
echo "<br>";
echo $p1 -> __toString();
echo "<br>";
echo $p2-> __toString();
?>

<?php
echo "<br>";
echo "Product Array:" . "<br>";

$products = [];
$products[] = $p1;
$products[] = $p2;

foreach ($products as $product){
    echo $product -> __toString() . "<br>";
}

?>

