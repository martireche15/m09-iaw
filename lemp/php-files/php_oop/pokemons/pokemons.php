<?php

class Pokemons
{
    // Atributos
    private $pokemons;

    // Constructor
    public function __construct()
    {
        $this->pokemons = [];
    }

    // Getters y setters
    public function getPokemons() {
        return $this->pokemons;
    }
    public function setPokemons($pokemons) {
        $this->pokemons = $pokemons;
    }

    // Añade un pokemon a la lista
    public function add_pokemon(Pokemon $pokemon)
    {
        $this->pokemons[] = $pokemon;
    }

    // Obtiene los pokemons de una generación específica
    public function get_generation($generation)
    {
        $result = [];
        foreach ($this->pokemons as $pokemon) {
            if ($pokemon->getGeneration() == $generation || $generation == "All") {
                $result[] = $pokemon;
            }
        }
        return $result;
    }

    // Obtiene los pokemons por nombre
    public function get_pokemon_name($text)
    {
        $result = [];
        foreach ($this->pokemons as $pokemon) {
            if (stripos($pokemon->getName(), $text) !== false) {
                $result[] = $pokemon;
            }
        }
        return $result;
    }

    // Obtiene los pokemons por tipo
    public function get_pokemon_type($text)
    {
        $result = [];
        foreach ($this->pokemons as $pokemon) {
            if (stripos($pokemon->getType1(), $text) !== false || stripos($pokemon->getType2(), $text) !== false) {
                $result[] = $pokemon;
            }
        }
        return $result;
    }

    public function get_code($text)
    {
        $result = [];
        foreach ($this->pokemons as $pokemon) {
            // Cambia `stripos` a una comparación directa para que la búsqueda sea exacta
            if ($pokemon->getCode() == $text) {
                $result[] = $pokemon;
            }
        }
        return $result;
    }

    // Método __toString
    public function __toString()
    {
        $result = "";
        foreach ($this->pokemons as $pokemon) {
            $result .= $pokemon . "\n";
        }
        return $result;
    }
}