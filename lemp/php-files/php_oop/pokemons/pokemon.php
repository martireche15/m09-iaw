<?php


class pokemon{
    private int $code;
    private string $name;
    private string $type1;
    private string $type2;
    private int $healthPoints;
    private int $attack;
    private int $defense;
    private int $specialAttack;
    private int $specialDefense;
    private int $speed;
    private int $generation;
    private bool $legendary;
    private string $image;
    private int $total;


    //constructor
    function __construct(int $code, string $name, string $type1,string $type2,int $healthPoints,int $attack,int $defense,int $specialAttack,$specialDefense ,int $speed,int $generation,$legendary, $image)
        {
        $this->code = $code;
        $this->name = $name;
        $this->type1 = $type1;
        $this->type2 = $type2;
        $this->healthPoints = $healthPoints;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->specialAttack = $specialAttack;
        $this->specialDefense = $specialDefense;
        $this->speed = $speed;
        $this->generation = $generation;
        $this->legendary = $legendary;
        $this->image = $image;
        $this->total = $this->total();
        }  
        public function total() {
            return $this->healthPoints + $this->attack + $this->defense + $this->specialAttack + $this->specialDefense + $this->speed;
        }
        public function getCode() {
            return $this->code;
        }
        public function setCode($code):void {
            $this->code = $code;
        }
        
        public function getName() {
            return $this->name;
        }
        public function setName($name):void {
            $this->name = $name;
        }
        
        public function getType1() {
            return $this->type1;
        }
        public function setType1($type1):void {
            $this->type1 = $type1;
        }
        
        public function getType2() {
            return $this->type2;
        }
        public function setType2($type2):void {
            $this->type2 = $type2;
        }
    
        public function getHealthPoints() {
            return $this->healthPoints;
        }
        public function setHealthPoints($healthPoints):void {
            $this->healthPoints = $healthPoints;
        }
    
        public function getAttack() {
            return $this->attack;
        }
        public function setAttack($attack):void {
            $this->attack = $attack;
        }
    
        public function getDefense() {
            return $this->defense;
        }
        public function setDefense($defense):void {
            $this->defense = $defense;
        }
    
        public function getSpecialAttack() {
            return $this->specialAttack;
        }
        public function setSpecialAttack($specialAttack):void {
            $this->specialAttack = $specialAttack;
        }
    
        public function getSpecialDefense() {
            return $this->specialDefense;
        }
        public function setSpecialDefense($specialDefense):void {
            $this->specialDefense = $specialDefense;
        }
    
        public function getSpeed() {
            return $this->speed;
        }
        public function setSpeed($speed):void {
            $this->speed = $speed;
        }
    
        public function getGeneration() {
            return $this->generation;
        }
        public function setGeneration($generation):void {
            $this->generation = $generation;
        }
    
        public function getLegendary() {
            return $this->legendary;
        }
        public function setLegendary($legendary):void {
            $this->legendary = $legendary;
        }
    
        public function getImage() {
            return $this->image;
        }
        public function setImage($image):void {
            $this->image = $image;
        }

        public function __toString():string {
            return "Code: {$this->code}, Name: {$this->name}, Type1: {$this->type1}, Type2: {$this->type2}, Health Points: {$this->healthPoints}, Attack: {$this->attack}, Defense: {$this->defense}, Special Attack: {$this->specialAttack}, Special Defense: {$this->specialDefense}, Speed: {$this->speed}, Generation: {$this->generation}, Legendary: {$this->legendary}, Image: {$this->image}, Total: {$this->total}";        }
}


