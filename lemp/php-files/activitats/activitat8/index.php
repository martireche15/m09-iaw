<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <h1>3000+ Peaks</h1>

    <?php

    $url = 'content.json'; // path to your JSON file
    $data = file_get_contents($url); // put the contents of the file into a variable
    
    // Check if data is retrieved successfully
    if ($data !== false) {
        $mountains = json_decode($data); // decode the JSON feed
        
        // Check if decoding was successful
        if ($mountains !== null) {
            // Loop through each mountain
            foreach ($mountains as $mountain) {
                // Print mountain data and image
                echo '<div class="mountaincontainer">';
                echo '<div class="mountain">';
                echo '<img src="' . $mountain->url .'">';
                echo '<div class="mountaininfo">';
                echo '<h4>' . $mountain->name . '</h4>';
                echo '<p>Height: ' . $mountain->height . ' meters</p>';
                echo '<p>Prominence: ' . $mountain->prominence . ' meters</p>';
                echo '<p>Zone: ' . $mountain->zone . '</p>';
                echo '<p>Country: ' . $mountain->country . '</p>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
        } else {echo '<p>Error decoding JSON data</p>';}
    } else {echo '<p>Error retrieving JSON data</p>';}

    ?>
</body>

</html>