<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div  class="container mt-4"> 
        <table class="table table-primary table-sm table-bordered">
            <?php
                include 'codi1.php';
            ?>
        </table>
        <table class="table table-success table-sm table-bordered">
            <?php
                include 'codi2.php';
            ?>
        </table>
        <div>
        <?php
                include 'codi3.php';
            ?>
        </div>
        <table class="table table-success table-sm table-bordered">
            <?php
                include 'codi4.php';
            ?>
        </table>
        <table class="table table-success table-sm table-bordered">
            <?php
                include 'codi5.php';
            ?>
        </table>
        <table class="table table-success table-sm table-bordered">
            <?php
                include 'codi6.php';
            ?>
        </table>
    </div>
</body>
</html>
