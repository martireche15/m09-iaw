<?php
    // Función para calcular el precio sin IVA
    function calcularPrecioSinIVA($precio_con_iva, $tasa_iva) {
    return $precio_con_iva / (1 + ($tasa_iva / 100));
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $precio_con_iva = $_POST["precio_con_iva"];
        $tasa_iva = $_POST["tasa_iva"];
    
        // Verificar que ambos campos no estén vacíos
        if (!empty($precio_con_iva) && !empty($tasa_iva)) {
            
            // Calcular el precio sin IVA
            $precio_sin_iva = calcularPrecioSinIVA($precio_con_iva, $tasa_iva);
    
            // Mostrar resultados
            echo "<p>Price without tax: $precio_sin_iva</p>";
            echo "<p>Round to 2 decimals using round(): " . round($precio_sin_iva, 2) . "</p>";
            echo "<p>Using function sprintf(): " . sprintf("%.4f", $precio_sin_iva) . "</p>";
        } else {
            echo "<p style='color: red;'>Por favor, complete ambos campos.</p>";
        }
    }    
?>

