<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Activitat 4</title>
</head>

<body>
    
    <div class="container mt-3">
        <h1>PRICE, TAX and ROUNDS</h1>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
            Price with TAX: <br><input type="text" name="precio_con_iva" required 
            value="<?php if(isset($_POST['precio_con_iva'])) echo $_POST['precio_con_iva']; ?>">
            <span class="error">*</span>
            <br><br>
            Tax (%): <br><input type="text" name="tasa_iva" required 
            value="<?php if(isset($_POST['tasa_iva'])) echo $_POST['tasa_iva']; ?>">
            <span class="error">*</span>
            <br><br><br>
            <input type="submit" name="submit" value="CALCULATE" class="button">
        </form>
        <br>
        <h3>TAX data:</h3>
        <?php include 'ex4.php'?>
    </div>

</body>
</html>

<!-- http://localhost:3003/activitats/activitat4/activitat4.php -->
