<!DOCTYPE html>
<html lang="en">
<head>
  <title>Activitat 5</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <div class="blue-content">
        <h2>Factorial Calculator: n!</h2> 
        <div class="border">
            <p>Enter a number to calculate the factorial n! using the calculator below.</p>                
            <?php include 'codi.php';?>
            <form method="POST">
                <label for="data">Number: </label>
                <input class="number" type="number" min="0" max="100" name="number" placeholder="Enter a number" value="<?php echo $number;?>">
                <br/><br/>
                <button class="boto" type="submit" name="execute" class="btn btn-primary">SEND</button>
            </form>
            <br>
            <div class="gris">
                <h4><b>Factorial Number:</b></h4>
                <p>n!= <?php echo execute();?></p>
            </div>
        </div>
        

        <br/><br/>
        <h4><b>Factorial Formulas</b></h4>
        <p>The formula to calculate a factorial for a number is:</p>
        <p>n! = n × (n-1) × (n-2) × ... × 1</p>
        <p>Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the
            number 1 is reached.</p>
        <p>The factorial of zero is 1:</p>
        <p>0! = 1</p>
        <br>
        <h4><b>Recurrence Relation</b></h6>
        <p>And the formula expressed using the recurrence relation looks like this:</p>
        <p>n! = n × (n – 1)!</p>
        <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
            until n is equal to 0.</p>
        <br>
        <h4><b>Factorial Table</b></h4>
        <p>The table below shows the factorial n! for the numbers one to one-hundred.</p>
        <div>
            <div><?php echo factorialTable();?></div> 
        </div>
    </div>
</body>
</html>

