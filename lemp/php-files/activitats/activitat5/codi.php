<?php
$text = "Enter a number to calculate the factorial n! using the calculator below." . "<br />";
$number = ""; 

if ($_POST) {
    if (isset($_POST['execute'])) {
        execute();
    }
}


function execute()
{
    $factorial = "";
    $number = "";
    if (isset($_POST['execute'])) {

        if (isset($_POST['number']) && $_POST['number'] != "") {
            $number = validate($_POST['number']);
            $factorial = factorial($number);
        }
        
        return $factorial;
    }
}


function validate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function factorial($valor) {
    if ($valor == 0) 
        return 1;
    $factorial = 1;
    for ($i = $valor; $i > 1; $i--) { 
        $factorial *= $i;
    }
    return $factorial;
}


function factorialTable(){
    echo "<table>";

        echo "<tr>";
            echo "<th>n</th>";
            echo "<th>n!</th>";
        echo "</tr>";

    for ($i = 0; $i <= 100; $i++) {

            echo "<tr>";    
                echo "<td>$i</td>";
                echo "<td>" . factorial($i) . "</td>";
            echo "</tr>";

    }
    echo "</table>";
}
?>