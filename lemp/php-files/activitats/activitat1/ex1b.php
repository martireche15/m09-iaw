<?php

$ventes = array(
    array ("Playstation 2", 155),
    array ("Nintendo DS", 154),
    array ("Game Boy", 119),
    array ("Play Station 4", 102),
    array ("Wii", 101),
    array ("Play Station 3", 87),
    array ("Xbox 360", 84),
    array ("Play Station Portable", 82),
    array ("Game Boy Advance", 81),
    array ("Xbox 360", 84),
    array ("Nintendo 3DS", 72),
    array ("Nes", 62),
    array ("Nintendo Switch", 60),
);

$max_limit = 100;

// Ordenar el array de mayor a menor según el número de ventas
usort($ventes, function($a, $b) {
    return $b[1] - $a[1];
});

echo "<table>";
echo "<tr><th>Consola</th><th>Barra</th><th>Vendes</th></tr>";

// Mostrar datos
foreach ($ventes as $consola) {
    echo "<tr>";
    echo "<td>" . $consola[0] . " : " . "</td>";
    $Factor = $consola[1] * 100 / 155;

    echo "<td>";
    $bar_lenght = min($consola[1] * 2, round($Factor));  // Corregir aquí
    
    echo str_repeat('<img src="green.png" alt="green" >', $bar_lenght);    
    echo "</td>";
    
    echo "<td>" . $consola[1] . ' Millions' . "</td>";
    
    echo "</tr>";
}

echo "</table>";
?>

