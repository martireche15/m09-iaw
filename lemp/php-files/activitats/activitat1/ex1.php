<?php

$ventes = array(
    array ("Playstation 2", 155),
    array ("Nintendo DS", 154),
    array ("Game Boy", 119),
    array ("Play Station 4", 102),
    array ("Wii", 101),
    array ("Play Station 3", 87),
    array ("Xbox 360", 84),
    array ("Play Station Portable", 82),
    array ("Game Boy Advance", 81),
    array ("Xbox 360", 84),
    array ("Nintendo 3DS", 72),
    array ("Nes", 62),
    array ("Nintendo Switch", 60),
);

$max_limit = 100;

echo "<table>";
echo "<thead><tr><th>Consola</th><th>Barra</th><th>Ventes</th></tr></thead>";

// Mostrar dades
    echo "<tbody>";
foreach ($ventes as $consola) {
    echo "<tr>";
    echo "<td>" . $consola[0] . " : " . "</td>";

    echo "<td>";
    $bar_lenght = min($consola[1] * 2, $max_limit);

    echo str_repeat('<img src="green.png" alt="green" >', $consola[1]);
    echo "</td>";
    
    echo "<td>" . $consola[1] . ' Millions' . "</td>";

    echo "</tr>";
}
    echo "</tbody>";

echo "</table>";
?>