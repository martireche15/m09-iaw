<?php
$servername = "172.21.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Successfully connected" . "<br>";
}

$sql = "SELECT book_id, title, isbn13, publication_date FROM book;";
$result = $conn->query($sql); // get the mysqli result

if ($result->num_rows > 0) {
    // output data of each row
    echo "Num rows: " . $result->num_rows;
    while ($row = $result->fetch_assoc()) {
        echo "book_id: " . $row["book_id"] . " - title: " . $row["title"] . ", " . $row["isbn13"] . ", " . $row["publication_date"] . "<br>";
    }
} else {
    echo "0 results";
}

//Close connection
$conn->close();
?>