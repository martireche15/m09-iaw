<?php
$servername = "172.21.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Successfully connected" . "<br>";
}

$sql = "SELECT book_id, title, isbn13, publication_date FROM book;";
$result = $conn->query($sql); // get the mysqli result

if ($result->num_rows > 0) {
    echo "<table border='1'>";
    echo "<tr><th>Book ID</th><th>Title</th><th>ISBN13</th><th>Publication Date</th></tr>";
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["book_id"] . "</td>";
        echo "<td>" . $row["title"] . "</td>";
        echo "<td>" . $row["isbn13"] . "</td>";
        echo "<td>" . $row["publication_date"] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

//Close connection
$conn->close();
?>