<!-- json_encode() -->

<?php
$age = array("Peter" => 35, "Ben" => 37, "Joe" => 43);
echo json_encode($age);
?>

<!-- 
OUTPUT: {"Peter":35,"Ben":37,"Joe":43} 
-->

