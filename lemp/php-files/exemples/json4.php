<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <?php

    //$jsonArray = '{"contacts":[{"name": "John", "age": 25}, {"name": "Jane", "age": 30}]}';

    $url = 'json4content.json'; // path to your JSON file
    $data = file_get_contents($url); // put the contents of the file into a variable
    $obj = json_decode($data); // decode the JSON feed

    //First register
    $obj1 = $obj->contacts[0];
    //Iterating first object $obj2
    foreach ($obj1 as $key => $value) {
        echo "[" . $key . ", " . $value . "]" . "<br>";
    }

    echo '<br />';
    //Iterating all $obj
    $obj = json_decode($data, true); // decode the JSON feed

    foreach ($obj["contacts"] as $contact) {
        foreach ($contact as $key => $value) {

            echo "[" . $key . ", " . $value . "]" . "<br>";
        }
    }

    echo '<br />';
    //Get a specified key-value
    foreach ($obj["contacts"] as $contact) {

        echo $contact['name'] . "<br>";
        echo $contact['age'] . "<br>";
    }
    ?>
</body>

</html>