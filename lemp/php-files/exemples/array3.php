<?php
// Associative arrays are arrays that use named keys that you assign to them.

$age['Peter'] = "35";
$age['Ben'] = "37";
$age['Joe'] = "43";

// $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

echo "Peter is " . $age['Peter'] . " years old.";
echo "<br/>";
echo count($age);
?> 