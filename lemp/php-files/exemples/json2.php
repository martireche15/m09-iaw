<!-- json_decode() -->

<?php
$jsonobj = '{"Peter":35,"Ben":37,"Joe":43}';

var_dump(json_decode($jsonobj));

$obj = json_decode($jsonobj);
//accessing to objects
echo "<br>";
echo $obj->Peter . "<br>";
echo $obj->Ben . "<br>";
echo $obj->Joe . "<br>";
echo "<br>";

//Iterating $obj
$obj = json_decode($jsonobj);
foreach ($obj as $key => $value) {
    echo "[" . $key . ", " . $value . "]" . "<br>";
}
?>