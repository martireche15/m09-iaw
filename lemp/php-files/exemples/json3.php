<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <?php

    //$jsonArray = '[{"name": "John", "age": 25}, {"name": "Jane", "age": 30}]';

    $url = 'json3content.json'; // path to your JSON file
    $data = file_get_contents($url); // put the contents of the file into a variable

    $array = json_decode($data, true);
    foreach ($array as $item) {
        echo $item['name'] . ", " . $item['age'] . "\n";
    }

    echo "<br>" . "<br>";

    foreach ($array as $item) {
        foreach ($item as $key => $value) {
            echo "[" . $key . ", " . $value . "]" . "<br>";
        }
    }

    ?>
</body>

</html>